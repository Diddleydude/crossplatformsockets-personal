
#include "StringUtils.h"
#include "SocketAddress.h"
#include "SocketAddressFactory.h"
#include "UDPSocket.h"
#include "TCPSocket.h"
#include "SocketUtil.h"

#include <iostream>

void echoClient();
void realEchoClient();

#if _WIN32
int main(int argc, const char** argv)
{
	echoClient();
	//	realEchoClient();
}
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

	echoClient();
	//	realEchoClient();
}
#endif

/* Sends one string to the echo server, get's that back and exits. OK for testing but
   this is not how a real client like telnet would work. */
void echoClient()
{
    SocketUtil::StaticInit();

	TCPSocketPtr client;
    client = SocketUtil::CreateTCPSocket(SocketAddressFamily::INET);

	string addressStr;

	std::cout << "### Echo Client ###" << std::endl;
	std::cout << "Enter host and port in the format host-ip:port e.g. 127.0.0.1:54321" << std::endl;
	std::cin >> addressStr;

	std::cout << "Connecting to: " << addressStr << std::endl;

    // Create a new socket address
    const SocketAddressPtr socketAddress = SocketAddressFactory::CreateIPv4FromString(addressStr);

    client->Connect(*socketAddress);

	std::cout << "Enter test return to send" << std::endl;
    string sendStr;
	std::cin >> sendStr;

    // send a string to the echo server.

  	int writeCount = client->Send(static_cast<const void*>(sendStr.c_str()),sendStr.length()+1);


	std::cout << "Wrote " << writeCount << " bytes." << std::endl;

	// the echo server will send it back

	const int BUFF_MAX = 32;
  	char buff[BUFF_MAX];
	char* buffHead;


	int readCount = 0;
	int totalRead = 0;

	// Set the place to read into the the start of the buffer.
	buffHead = buff;
	do
	{
		/* If we loose the CPU (block) we'll retun from the Receive, need to
		keep reading until we've go the whole message */
		// buffHead - current point we're reading into from the network
		//BUFF_MAX-totalRead - tracking the amount of buffer used.
		readCount = client->Receive(static_cast<void*>(buffHead),BUFF_MAX-totalRead);

		// Move the place to read to along in memroy
		buffHead+=readCount;

		//std::cout << "Read " << readCount << " bytes of " << writeCount << " bytes." << std::endl;

		totalRead += readCount;
	} while(totalRead < writeCount);

	/* Note: This only works for echo servers, we know in advance how
	much data we'll need to recieve as its the same as we sent */

	buff[totalRead] = 0; //pull the terminating character back.

	string recStr(buff);
	std::cout << recStr << std::endl;

	client.reset();
	SocketUtil::CleanUp();
}

/* A more realistic client, this one allows us to send multiple strings until we enter an exit character. 
   This is the way text based terminal clients like telnet work */
void realEchoClient()
{
	SocketUtil::StaticInit();

	TCPSocketPtr client;
    client = SocketUtil::CreateTCPSocket(SocketAddressFamily::INET);

	string addressStr;

	std::cout << "### Echo Client ###" << std::endl;
	std::cout << "Enter host and port in the format host-ip:port e.g. 127.0.0.1:54321" << std::endl;
	std::cin >> addressStr;

	std::cout << "Connecting to: " << addressStr << std::endl;

    // Create a new socket address
    const SocketAddressPtr socketAddress = SocketAddressFactory::CreateIPv4FromString(addressStr);

    client->Connect(*socketAddress);


  	string sendStr;
	const int BUFF_MAX = 32;
	char buff[BUFF_MAX];
	char* buffHead;

	std::cout << "Enter text, return to send. To exit ^] (Ctrl-])" << std::endl;
	std::cin >> sendStr;

	while(sendStr.compare(0,1,"\u001d") !=0)
	{
		// send a string to the echo server.

	  	int writeCount = client->Send(static_cast<const void*>(sendStr.c_str()),sendStr.length()+1);


		//std::cout << "Wrote " << writeCount << " bytes." << std::endl;

		// the echo server will send it back

		int readCount = 0;
		int totalRead = 0;

		// Set the place to read into the the start of the buffer.
		buffHead = buff;
		do
		{
			/* If we loose the CPU (block) we'll retun from the Receive, need to
			keep reading until we've go the whole message */
			// buffHead - current point we're reading into from the network
			//BUFF_MAX-totalRead - tracking the amount of buffer used.
			readCount = client->Receive(static_cast<void*>(buffHead),BUFF_MAX-totalRead);

			// Move the place to read to along in memroy
			buffHead+=readCount;

			//std::cout << "Read " << readCount << " bytes of " << writeCount << " bytes." << std::endl;

			totalRead += readCount;
		} while(totalRead < writeCount);

		/* Note: This only works for echo servers, we know in advance how
		much data we'll need to recieve as its the same as we sent */

		buff[totalRead] = 0; //pull the terminating character back.

		string recStr(buff);
	    std::cout << recStr << std::endl;

		std::cout << "Enter text, return to send. To exit ^] (Ctrl-])" << std::endl;
		std::cin >> sendStr;
	}

	client.reset();
	SocketUtil::CleanUp();
}
