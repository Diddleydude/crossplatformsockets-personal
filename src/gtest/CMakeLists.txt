#-------------------
# Google Test
#-------------------
# Updated based on https://google.github.io/googletest/quickstart-cmake.html
include(FetchContent)

FetchContent_Declare(
  googletest
  URL https://github.com/google/googletest/archive/e2239ee6043f73722e7aa812a459f54a28552929.zip
)

# On Windows make sure you don't try and use pthreads 
if(WIN32)
	set(gtest_disable_pthreads ON)
	set(CMAKE_USE_WIN32_THREADS_INIT 1)
	set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
endif()

FetchContent_GetProperties(googletest)
if(NOT googletest_POPULATED)
  FetchContent_Populate(googletest)
  message(STATUS "GoogleTest source dir: ${googletest_SOURCE_DIR}")
  message(STATUS "GoogleTest binary dir: ${googletest_BINARY_DIR}")
  add_subdirectory(${googletest_SOURCE_DIR} ${googletest_BINARY_DIR})
endif()

# requires cmake 3.14
#FetchContent_MakeAvailable(googletest)

include_directories(${COMMON_INCLUDES})

add_definitions(${MSVC_COMPILER_DEFS})

enable_testing()

set(PROJECT_TEST_NAME ${PROJECT_NAME_STR}_test)
file(GLOB TEST_SRC_FILES ${PROJECT_SOURCE_DIR}/src/gtest/*.cpp)

# Construct the testing executable
add_executable(${PROJECT_TEST_NAME} ${TEST_SRC_FILES})

# This forces names & gtest to be built if mytests is the target.

#I'm not sure how your supposed to figure out target names, i did a cmake <tab>
#with no dependencies to get it work.
add_dependencies(${PROJECT_TEST_NAME} gtest) # google test target
add_dependencies(${PROJECT_TEST_NAME} strings) # my target(s)
add_dependencies(${PROJECT_TEST_NAME} networking)

target_include_directories(${PROJECT_TEST_NAME} PUBLIC ${googletest_SOURCE_DIR}/googletest/include)

# this is target inside googletest
target_link_libraries(${PROJECT_TEST_NAME} PUBLIC gtest)

# this is my target under test
target_link_libraries (${PROJECT_TEST_NAME} LINK_PUBLIC strings)
target_link_libraries (${PROJECT_TEST_NAME} LINK_PUBLIC networking)

include(GoogleTest)
gtest_discover_tests(${PROJECT_TEST_NAME})